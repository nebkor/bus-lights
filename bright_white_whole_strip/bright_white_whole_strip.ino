
#include <Adafruit_DotStar.h>
// Because conditional #includes don't work w/Arduino sketches...
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
//#include <avr/power.h> // ENABLE THIS LINE FOR GEMMA OR TRINKET

#define NUMPIXELS 150 // Number of LEDs in strip

// Here's how to control the LEDs from any two pins:
#define DATAPIN    11
#define CLOCKPIN   13
Adafruit_DotStar strip = Adafruit_DotStar(NUMPIXELS, DOTSTAR_BGR);

void setup() {
  strip.begin();
  strip.show();
}

int      head  = 0;

unsigned int color[] = {255, 255, 255};

void loop() {

  strip.setPixelColor(head, color[0], color[1], color[2]); // 'On' pixel at head

  head = (head + 1) % NUMPIXELS;

  if(head == 0) {
    strip.show();
  }
}
