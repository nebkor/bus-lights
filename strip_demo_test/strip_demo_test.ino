
#include <Adafruit_DotStar.h>
// Because conditional #includes don't work w/Arduino sketches...
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
//#include <avr/power.h> // ENABLE THIS LINE FOR GEMMA OR TRINKET

#define NUMPIXELS 150 // Number of LEDs in strip

// Here's how to control the LEDs from any two pins:
#define DATAPIN    11
#define CLOCKPIN   13
Adafruit_DotStar strip = Adafruit_DotStar(NUMPIXELS, DOTSTAR_GRB);

void setup() {
  strip.begin();
  strip.show();
}

int      head  = 0;
//uint32_t color = 0x000000;      // 'On' color (starts red)

unsigned int color[] = {255, 0, 0};

void loop() {

  strip.setPixelColor(head, color[0], color[1], color[2]); // 'On' pixel at head
  //strip.setPixelColor(tail, 0);     // 'Off' pixel at tail
  //strip.show();                     // Refresh strip
  //delay(10);                        // Pause 20 milliseconds (~50 FPS)
  //strip.show();
  //color += 1;

  for (int dec = 0; dec < 3; ++dec) {
    int inc = dec == 2 ? 0 : dec + 1;

    // cross-fade the two colours.
    for(int i = 0; i < 255; i += 1) {
      color[dec] -= 1;
      color[inc] += 1;
      
      strip.show();
    }
  }
  //delay(5);

  if(head == 0) {
    //strip.show();
//    color = color + 1;
//    if((color >>= 8) == 0) {
//      color = 0xFF0000;
//    }
  }

  //delay(20);

  head = (head + 1) % NUMPIXELS;

  /* if(++head > NUMPIXELS) {         // Increment head index.  Off end of strip?
    head = 0;                       //  Yes, reset head index to start
    //strip.show();
    //delay(500);
    //color += 1;
    if((color >>= 8) == 0) {
      color = 0xFF0000; 
    }
  } */
}
