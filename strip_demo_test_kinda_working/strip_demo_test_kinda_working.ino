
#include <Adafruit_DotStar.h>
// Because conditional #includes don't work w/Arduino sketches...
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
//#include <avr/power.h> // ENABLE THIS LINE FOR GEMMA OR TRINKET

#define NUMPIXELS 150 // Number of LEDs in strip

// Here's how to control the LEDs from any two pins:
#define DATAPIN    11
#define CLOCKPIN   6
Adafruit_DotStar strip = Adafruit_DotStar(NUMPIXELS, DOTSTAR_BGR);

void setup() {
  strip.begin();
  strip.show();
}

int      head  = 0;
//uint32_t color = 0x000000;      // 'On' color (starts red)

unsigned int color[] = {255, 0, 0};

void loop() {

  //strip.setPixelColor(head, color[0], color[1], color[2]); // 'On' pixel at head

  for (int dec = 0; dec < 3; ++dec) {
    int inc = dec == 2 ? 0 : dec + 1;

    // cross-fade the two colours.
    for(int i = 0; i < 255; i += 1) {
      color[dec] -= 1;
      color[inc] += 1;
      
      for(int j = 0 ; j < NUMPIXELS; ++j) {
        strip.setPixelColor(head, color[0], color[1], color[2]);
        head = (head + 1) % NUMPIXELS;
      }
      strip.show();

      delay(700);
    }
  }

  if(head == 0) {
    //strip.show();
  }

  //delay(20);

//  head = (head + 1) % NUMPIXELS;

  /* if(++head > NUMPIXELS) {         // Increment head index.  Off end of strip?
    head = 0;                       //  Yes, reset head index to start
    //strip.show();
    //delay(500);
    //color += 1;
    if((color >>= 8) == 0) {
      color = 0xFF0000; 
    }
  } */
}

